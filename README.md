# Parallel Quick-Sort using MPJ

Parallel Quick-Sort as described here, on page 14:
https://www.uio.no/studier/emner/matnat/ifi/INF3380/v10/undervisningsmateriale/inf3380-week12.pdf

Implemented using MPJ:
http://mpj-express.org/index.html